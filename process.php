<?php
// Validate input
if (isset($_POST['domain']) && isset($_POST['ip'])) {
    $domain = htmlspecialchars($_POST['domain']);
    $ip = htmlspecialchars($_POST['ip']);

    // Generate a unique identifier for the temporary URL
    $identifier = uniqid();

    // Create a temporary file to store the domain and IP mapping
    $mappingFile = "mappings/{$identifier}.json";
    $mappingData = [
        'domain' => $domain,
        'ip' => $ip,
    ];
    file_put_contents($mappingFile, json_encode($mappingData));

    // Generate the temporary URL
    $temporaryUrl = "http://{$_SERVER['HTTP_HOST']}/redirect.php?id={$identifier}";

    echo "<h2>Your Temporary URL:</h2>";
    echo "<p><a href='{$temporaryUrl}'>{$temporaryUrl}</a></p>";
} else {
    echo "Invalid input.";
}
?>
