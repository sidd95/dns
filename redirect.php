<?php
// Validate input
if (isset($_GET['id'])) {
    $identifier = htmlspecialchars($_GET['id']);

    // Load the mapping data
    $mappingFile = "mappings/{$identifier}.json";
    if (file_exists($mappingFile)) {
        $mappingData = json_decode(file_get_contents($mappingFile), true);

        // Redirect to the specified IP address
        header("Location: http://{$mappingData['ip']}", true, 302);
        exit;
    }
}

// Invalid or missing identifier
echo "Invalid request.";
?>
