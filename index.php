<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Temporary Domain Pointing</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            text-align: center;
            padding: 50px;
        }
        h1 {
            color: #333;
        }
        form {
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <h1>Temporary Domain Pointing</h1>
    <form action="process.php" method="post">
        <label for="domain">Domain:</label>
        <input type="text" id="domain" name="domain" required>
        <br>
        <label for="ip">IP Address:</label>
        <input type="text" id="ip" name="ip" required>
        <br>
        <button type="submit">Generate Temporary URL</button>
    </form>
</body>
</html>
